var mysql = require('mysql2');
var AWS = require('aws-sdk');
var _ = require('underscore');

const path = require('path');
const args = require('yargs').argv;
const storage = require('azure-storage');

var config =
{
    host     : process.env.CUSTOMCONNSTR_RDS_HOSTNAME,
    user     : process.env.CUSTOMCONNSTR_RDS_USERNAME,
    password : process.env.CUSTOMCONNSTR_RDS_PASSWORD,
    port     : process.env.CUSTOMCONNSTR_RDS_PORT,
    database : process.env.CUSTOMCONNSTR_RDS_DB_NAME,
    ssl: true
};

// Candidate Side Methods
exports.submitResume = function(req, res) {
  const blobService = storage.createBlobService();
  const containerName = 'test-resume-container';

  const sourceFilePath = path.resolve(req.params.filename);
  const blobName = path.basename(sourceFilePath, path.extname(sourceFilePath));

  const upload = () => {
      return new Promise((resolve, reject) => {
          blobService.createBlockBlobFromLocalFile(containerName, blobName, sourceFilePath, err => {
              if (err) {
                  reject(err);
              } else {
                  resolve({ message: `Upload of '${blobName}' complete` });
                  res.status(200).send(JSON.stringify(blobName));
              }
          });
      });
  };
};

exports.submitInterviewDetails = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var candidateSearchQuery =
         "select c.id as cid " +
         "from Candidate c inner join CandidateContactInformation cCon "+
           "on c.id = cCon.candidate_id " +
         "where c.first_name = '" +req.params.candFName+"' and cCon.contact_value = '"+
           req.params.candEmail+"'";

       conn.query(candidateSearchQuery, function(err, rows, fields) {
         if (err) throw err;
         var candidateId = 50;
         var candidateInsertQuery = "insert into Candidate SET ?";
         var candPost  = {first_name: req.params.candFName, middle_name: req.params.candMName,
           last_name: req.params.candLName,
           resume_dl_link: req.params.candResume };
         if(rows.length > 0) {
           candidateId = rows[0]['cid'];

           var candContactUpdateQuery = "update CandidateContactInformation SET ? WHERE Contact_Type = 1 and Candidate_Id = ?";
           var candEmailPost  = {Contact_Value: req.params.candEmail};

           conn.query(candContactUpdateQuery, [candEmailPost, candidateId], function(err, result) {
             if (err) throw err;

             var candContactUpdateQuery = "update CandidateContactInformation SET ? WHERE Contact_Type = 2 and Candidate_Id = ?";
             var candPhonePost  = {Contact_Value: req.params.candPhone};

             conn.query(candContactUpdateQuery, [candPhonePost, candidateId], function(err, result) {
               if (err) throw err;

               var candResumeUpdateQuery = "update Candidate SET ? WHERE id = ?";
               var candResumePost  = {resume_dl_link: req.params.candResume};

               conn.query(candResumeUpdateQuery, [candResumePost, candidateId], function(err, result) {

                 var interviewUpdateQuery = "update AppointmentSchedule SET ? WHERE "+
                   "start_date = ? and start_time = ? and appointment_desc = 'available' and "+
                   "recruiter_id = ? and event_id = ?";
                 var interviewPost = {appointment_desc: 'Interview', candidate_id: candidateId};

                 conn.query(interviewUpdateQuery,
                   [interviewPost, req.params.intDate, req.params.intStart, req.params.intId,req.params.eventId],
                   function(err, result) {
                     if (err) throw err;
                     if(result['changedRows'] == 0) {
                       throw err;
                     } else {
                       res.status(200).send();
                     }
                   }
                 );
               });
             });
           });
         } else {
           conn.query(candidateInsertQuery, candPost, function(err, result) {
             if (err) throw err;

             var candidateSmallSearchQuery =
               "select c.id as cid " +
               "from Candidate c "+
               "where c.first_name = '" +req.params.candFName+"' and "+
                 "c.middle_name = '" +req.params.candMName+"' and "+
                 "c.last_name = '" +req.params.candLName+"' "+
               "order by cid asc";
             var candidateId = 50;

             conn.query(candidateSmallSearchQuery, function(err, rows, fields) {
               candidateId = rows[rows.length-1]['cid'];

               var candContactInsertQuery = "insert into CandidateContactInformation SET ?";
               var candEmailPost  = {Candidate_Id: candidateId, Contact_Type: 1,
                 Contact_Value: req.params.candEmail};

               conn.query(candContactInsertQuery, candEmailPost, function(err, result) {
                 if (err) throw err;

                 var candContactInsertQuery = "insert into CandidateContactInformation SET ?";
                 var candPhonePost  = {Candidate_Id: candidateId, Contact_Type: 2,
                   Contact_Value: req.params.candPhone};

                 conn.query(candContactInsertQuery, candPhonePost, function(err, result) {
                   if (err) throw err;

                   var interviewUpdateQuery = "update AppointmentSchedule SET ? WHERE "+
                     "start_date = ? and start_time = ? and appointment_desc = 'available' and "+
                     "recruiter_id = ? and event_id = ?";
                   var interviewPost = {appointment_desc: 'Interview', candidate_id: candidateId};

                   conn.query(interviewUpdateQuery,
                     [interviewPost, req.params.intDate, req.params.intStart, req.params.intId,req.params.eventId],
                     function(err, result) {
                       if (err) throw err;
                       if(result['changedRows'] == 0) {
                         throw err;
                       } else {
                         res.status(200).send();
                       }
                     }
                   );
                 });
               });
             });
           });
         }
       });
    }
  });
};

// Interviewer Side Methods
exports.submitEventDetails = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var eventInsertQuery = "insert into Event SET ?";
       var eventPost  = { name: req.params.eventName, start_date: req.params.eventStartDate,
         end_date: req.params.eventEndDate, start_time: req.params.eventStartTime,
         end_time: req.params.eventEndTime };

       conn.query(eventInsertQuery, eventPost, function(err, result) {
         if (err) throw err;

         var eventIdSearchQuery =
           "select e.id as eid " +
           "from Event e "+
           "where e.name = '" +req.params.eventName+"' and "+
             "e.start_date = '" +req.params.eventStartDate+"'";
         var eventId = 0;

         conn.query(eventIdSearchQuery, function(err, rows, fields) {
           if (err) throw err;

           var eventRows = rows;
           eventId = eventRows[0]['eid'];
           var roomName = "";
           var eventRoomInsertQuery = "insert into EventRooms SET ?";
           for(var newRmIter = 0; newRmIter < req.params.eventRoomList.length; newRmIter++) {
             if( req.params.eventRoomList[newRmIter] != ',') {
               roomName += req.params.eventRoomList[newRmIter];
             } else {
               var rmPost  = {event_id: eventId, room: roomName };
               conn.query(eventRoomInsertQuery, rmPost, function(err, result) {
                 if (err) throw err;
               });
               roomName = "";
             }
           }
           var rmPost  = {event_id: eventId, room: roomName };
           conn.query(eventRoomInsertQuery, rmPost, function(err, result) {
             if (err) throw err;
           });
           res.status(200).send(JSON.stringify(eventRows));
         });
       });
    }
  });
};

exports.submitEventRecruiters = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       console.log(  req.body );

       if(  _.keys(req.body.recruiterFirstName).length > 1 ) {
         for(var newRecIter = 0; newRecIter < req.body.recruiterFirstName.length; newRecIter++) {
           var recruiterId = 50;
           var recruiterInsertQuery = "insert into Recruiter SET ?";
           var recPost  = {first_name: req.body.recruiterFirstName[newRecIter],
             middle_name: req.body.recruiterMiddleName[newRecIter],
             last_name: req.body.recruiterLastName[newRecIter], email_id: req.body.recruiterLogin[newRecIter] };

           conn.query(recruiterInsertQuery, recPost, function(err, result) {
             if (err) throw err;
           });

           var recruiterSearchQuery =
             "select r.id as rid " +
             "from Recruiter r " +
             "where r.email_id = '" +req.body.recruiterLogin[newRecIter]+"'";
           var recruiterId = 50;
           var interviewRoomId = req.body.interviewRoom[newRecIter];

           conn.query(recruiterSearchQuery, function(err, rows, fields) {
             recruiterId = rows[rows.length-1]['rid'];

             var registeredEventRecInsertQuery = "insert into RegisteredEventRecruiters SET ?";
             var regRecPost  = {event_id: req.params.eventId, recruiter_id: recruiterId,
               def_interview_room_id: interviewRoomId};

             conn.query(registeredEventRecInsertQuery, regRecPost, function(err, result) {
               if (err) throw err;
             });
           });
         }
       } else {
         var recruiterId = 50;
         var recruiterInsertQuery = "insert into Recruiter SET ?";
         var recPost  = {first_name: req.body.recruiterFirstName,
           middle_name: req.body.recruiterMiddleName,
           last_name: req.body.recruiterLastName, email_id: req.body.recruiterLogin };

         conn.query(recruiterInsertQuery, recPost, function(err, result) {
           if (err) throw err;
         });

         var recruiterSearchQuery =
           "select r.id as rid " +
           "from Recruiter r " +
           "where r.email_id = '" +req.body.recruiterLogin+"'";
         var recruiterId = 50;

         conn.query(recruiterSearchQuery, function(err, rows, fields) {
           recruiterId = rows[rows.length-1]['rid'];

           var registeredEventRecInsertQuery = "insert into RegisteredEventRecruiters SET ?";
           var regRecPost  = {event_id: req.params.eventId, recruiter_id: recruiterId,
             def_interview_room_id: req.body.interviewRoom};

           conn.query(registeredEventRecInsertQuery, regRecPost, function(err, result) {
             if (err) throw err;
           });
         });
       }
    }
  });
  res.redirect('/#/event/'+req.params.eventId+'/eventSubmitted');
};
exports.submitRecruiterTime = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       console.log(  _.keys(req.body.busyDate).length );

       if(  _.keys(req.body.busyDate).length > 1 ) {
         for(var newTimeIter = 0; newTimeIter < req.body.busyDate.length; newTimeIter++) {
           if( req.body.interviewStart[newTimeIter] != '' ) {
             var interviewUpdateQuery = "update AppointmentSchedule SET ? WHERE "+
                 "start_date = ? and start_time = ? and recruiter_id = ? and event_id = ?";
               var interviewPost = {appointment_desc: req.params.timeType, candidate_id: 1};

               conn.query(interviewUpdateQuery,
                 [interviewPost, req.body.busyDate[newTimeIter], req.body.interviewStart[newTimeIter],
                 req.params.recruiterId, req.params.eventId],
                 function(err, result) {
                   if (err) throw err;
                 }
               );
           } else {
             var interviewUpdateQuery = "update AppointmentSchedule SET ? WHERE appointment_desc != 'Interview' "+
                 "and start_date = ? and start_time >= ? and end_time <= ? and recruiter_id = ? and event_id = ?";
               var interviewPost = {appointment_desc: req.params.timeType, candidate_id: 1};

               conn.query(interviewUpdateQuery,
                 [interviewPost, req.body.busyDate[newTimeIter], req.body.interviewRangeStart[newTimeIter],
                 req.body.interviewRangeEnd[newTimeIter], req.params.recruiterId, req.params.eventId],
                 function(err, result) {
                   if (err) throw err;
                 }
               );
           }
         }
       } else {
         if( req.body.interviewStart != '' ) {
           var interviewUpdateQuery = "update AppointmentSchedule SET ? WHERE "+
               "start_date = ? and start_time = ? and recruiter_id = ? and event_id = ?";
           var interviewPost = {appointment_desc: req.params.timeType, candidate_id: 1};

           conn.query(interviewUpdateQuery,
             [interviewPost, req.body.busyDate, req.body.interviewStart,
             req.params.recruiterId, req.params.eventId],
             function(err, result) {
               if (err) throw err;
             }
           );
         } else {
           var interviewUpdateQuery = "update AppointmentSchedule SET ? WHERE appointment_desc != 'Interview' "+
           "and start_date = ? and start_time >= ? and end_time <= ? and recruiter_id = ? and event_id = ?";
           var interviewPost = {appointment_desc: req.params.timeType, candidate_id: 1};

           conn.query(interviewUpdateQuery,
             [interviewPost, req.body.busyDate, req.body.interviewRangeStart,
             req.body.interviewRangeEnd, req.params.recruiterId, req.params.eventId],
             function(err, result) {
               if (err) throw err;
             }
           );
         }
       }
    }
  });
  res.redirect('/#/timeSubmitted');
};
