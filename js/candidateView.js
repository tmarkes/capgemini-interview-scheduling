angular.module("CandidateController", [])
.factory("interview", function() {
    var interview = 	{ "id" : 0, "interviewer": 0, "date" : "",
      "startTime": "", "endTime": ""};

    return {}
})
.factory("candidate", function() {
    var candidate = 	{ "id" : 0, "firstname": "", "middlename" : "", "lastname": "",
      "phone": "", "email": "", "resumeLink": ""};

    return {}
})
.controller("ChooseEventCtrl", function($scope, $state, $http, userEvent) {
    $http({
          method: 'GET',
          url: '/retrieve/event'
    })
    .success(function(data, status, headers, config) {
         $scope.items = data;
         $scope.selectionItem = "an Event";
    });

    $scope.processChoice = function(itemId,itemName) {
      userEvent.addEvent(itemId);
      $state.go("scheduleInterview", { eventId : itemId });
    }
})
.controller("AddInterviewCtrl", function($scope, $state, $http, userEvent, candidate, interview) {
    $scope.candidate = candidate;
    $scope.interview = interview;

    $scope.$watch("interview.date", function(newValue, oldValue){
      $http({
            method: 'GET',
            url: '/retrieve/event/'+userEvent.getEvent()+
              '/'+newValue+'/availableTimes'
      })
      .success(function(data, status, headers, config) {
           $scope.interviewers = data;
      });
    });

    $scope.saveInterviewDetails = function(interview, candidate) {
      var filename = 'noResumesNow.pdf';
      /*  if($(' #candidateResume ').get(0).files.length == 0) {
          alert('Please submit a resume with your interview');
          return true;
        }
        if($(' #candidateResume ').get(0).files[0].size > 10585760) {
          alert('Sorry, file size must be under 10MB');
          return true;
        }*/

        // Needs to go to '@' symbol
    //    var filename = $scope.candidate.email.split('@')[0]+"_"+
    //      $(' #candidateResume ').get(0).files[0].name.split('.')[0].replace(/ /g,"_");
    /*    var filetype = encodeURIComponent($(' #candidateResume ').get(0).files[0].type);
        $http.post('/presigned/'+filename+'/'+filetype)
        .success(function(resp) {
          // Perform The Push To S3
          $http.put(decodeURI(resp.replace(/['"]+/g, '')), $(' #candidateResume ').get(0).files[0],
              {headers:
                { 'Content-Type': filetype.replace('%2F','/'),
                  'Access-Control-Allow-Origin' : '*',
                  'Access-Control-Allow-Headers': 'X-Requested-With'
              }}
          )
          .success(function(resp) { */
              //Resume upload complete. Now upload interview data.
              var detailsUrl = '/submit/event/'+userEvent.getEvent()+'/interview'+
                '/'+$scope.interview.startTime.id+'/'+$scope.interview.date+
                '/'+$scope.interview.startTime.timeId+'/'+$scope.candidate.firstname+
                '/'+$scope.candidate.middlename+'/'+$scope.candidate.lastname+
                '/'+$scope.candidate.phone+'/'+$scope.candidate.email+
                '/'+filename;
              $http({
                    method: 'POST',
                    url: detailsUrl
              })
              .success(function(data, status, headers, config) {
                  $http({
                        method: 'GET',
                        url: '/sendEmails/'+$scope.candidate.email+"/"+userEvent.getEvent()+"/"
                          +$scope.interview.date+"/"+$scope.interview.startTime.timeId+"/"
                          +$scope.interview.startTime.id+"/"+$scope.candidate.firstname+"/"
                          +$scope.candidate.lastname
                  })
                  .error(function(resp) {
                       console.log(resp);
                  });
                  $state.go("interviewConfirm");
              });
        /*  })
          .error(function(resp) {
            alert("Selected interview time is no longer available. Please submit a new one.");
            console.log(resp);
            $http({
                  method: 'GET',
                  url: '/retrieve/event/'+userEvent.getEvent()+
                    '/'+$scope.interview.date+'/availableTimes'
            });
          })
      })
      .error(function(resp) {
          console.log(resp);
          alert("Selected interview time is no longer available. Please submit a new one.");
          $http({
                method: 'GET',
                url: '/retrieve/event/'+userEvent.getEvent()+
                  '/'+$scope.interview.date+'/availableTimes'
          });
      });*/
    }

    $scope.goToInterviewersList = function() {
      $state.go("viewInterviewers", { eventId : userEvent.getEvent() });
    }
});
