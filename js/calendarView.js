/**
 * Created by ngottam on 3/31/2016.
 */
angular.module("CalendarController", ['ui.calendar'])
    .factory("interviewer", function($http) {
        var interviewer = 	{ "id" : 0, "name": "", "eventId" : 0 };
        return {}
    })
    .controller("calendarCtlr", function ($scope, $state, $http, interviewer) {
        $scope.interviewer = interviewer;

        $http({
              method: 'GET',
              url: '/retrieve/event/'+$state.params.eventId+
                      '/recruiter/'+$state.params.recruiterId+
                      '/calendar'
        })
        .success(function(data, status, config, headers, interviewer) {
            $scope.interviewer.name = $state.params.recruiterName;
            $scope.interviewer.eventId = $state.params.eventId;
            $scope.interviewer.id = $state.params.recruiterId;

            // Add retrieved calendar events
            var newEventOpts = [];
            var eventEntry = '';
            for( var i = 0; i < data.length; i++ ) {
              eventEntry = {
                title: data[i]['appointment_desc'],
                stick: true,
                start: new Date(
                  parseInt(data[i]['start_year']),parseInt(data[i]['start_month'])-1,
                  parseInt(data[i]['start_day']),parseInt(data[i]['start_hour']),
                  parseInt(data[i]['start_min'])),
                end: new Date(
                  parseInt(data[i]['end_year']),parseInt(data[i]['end_month'])-1,
                  parseInt(data[i]['end_day']),parseInt(data[i]['end_hour']),
                  parseInt(data[i]['end_min'])),
                allDay: false,
                url: "https://s3.amazonaws.com/"+data[i]['resume']
              };
              newEventOpts.splice(i,0,eventEntry);
            }
            for(var i = 0; i < newEventOpts.length; ++i) {
              console.log(newEventOpts[i]);
              $scope.eventOptions.push([newEventOpts[i]]);
            }
        });

        var eventOpts = [];
        $scope.eventOptions = [eventOpts];
        $scope.eventClicked = function(item){
          var event = Object(item);
          var messageField = document.getElementById("alertMessageWhenEventIsClicked");
          messageField.innerHTML = "Resume:" + event.name  ;
        };
        $scope.calOptions = {
          editable:false,
          header:{
            left:'prev, today',
            center:'title',
            right:'agendaWeek agendaDay next'
          },
          nextDayThreshold:'00:00:00'
        };
    })
    .controller('busyTimeCalendarCtrl', function($scope, $state, $http, $controller, userEvent) {
      $http({
            method: 'GET',
            url: '/retrieve/event/'+$state.params.eventId+
                    '/recruiter/'+$state.params.recruiterId+
                    '/calendar'
      })
      .success(function(data, status, config, headers) {

          // Add retrieved calendar events
          var newEventOpts = [];
          var eventEntry = '';
          for( var i = 0; i < data.length; i++ ) {
            eventEntry = {
              title: data[i]['appointment_desc'],
              stick: true,
              start: new Date(
                parseInt(data[i]['start_year']),parseInt(data[i]['start_month'])-1,
                parseInt(data[i]['start_day']),parseInt(data[i]['start_hour']),
                parseInt(data[i]['start_min'])),
              end: new Date(
                parseInt(data[i]['end_year']),parseInt(data[i]['end_month'])-1,
                parseInt(data[i]['end_day']),parseInt(data[i]['end_hour']),
                parseInt(data[i]['end_min'])),
              allDay: false,
              url: "https://s3.amazonaws.com/"+data[i]['resume']
            };
            newEventOpts.splice(i,0,eventEntry);
          }
          for(var i = 0; i < newEventOpts.length; ++i) {
            console.log(newEventOpts[i]);
            $scope.eventOptions.push([newEventOpts[i]]);
          }
      });

      var eventOpts = [];
      $scope.eventOptions = [eventOpts];
      $scope.eventClicked = function(item){
        var event = Object(item);
        var messageField = document.getElementById("alertMessageWhenEventIsClicked");
        messageField.innerHTML = "Resume:" + event.name  ;
      };
      $scope.calOptions = {
        editable:false,
        header:{
          left:'prev, today',
          center:'title',
          right:'agendaWeek agendaDay next'
        },
        nextDayThreshold:'00:00:00'
      };

      $scope.interviewerName = $state.params.recruiterName;
      $scope.specificTimeWindow = 'Specific Time Window';
      $scope.timeRange = 'Time Range';
      $scope.windowInterviewers = null;
      $scope.rangeInterviewers = null;
      $http({
            method: 'GET',
            url: '/retrieve/event/'+userEvent.getEvent()
      })
      .success(function(data, status, headers, config) {
           $scope.eventName = data[0]['name'];
           $scope.eventStartDate = data[0]['start_date'].split("T")[0];
           $scope.eventEndDate = data[0]['end_date'].split("T")[0];
           $scope.eventStartTime = data[0]['start_time'];
           $scope.eventEndTime = data[0]['end_time'];
      });

      $scope.times = [
        { id: 'time1', dateChanged: false, windowInterviewers: $scope.windowInterviewers,
        rangeInterviewers: $scope.rangeInterviewers
        }
      ];

      $scope.$watch("unavailableTimeForm.busyDate", function(newValue, oldValue){
        $(".newTimeslotForm").each(function() {
          if((
             $(this).find(".interviewStartTime :selected").text().includes("Select One")
          || $(this).find(".interviewRangeStartTime :selected").text().includes("Select One")
          || $(this).find(".interviewRangeEndTime :selected").text().includes("Select One")
              )
             && $(this).find(".busyDate").text() !== oldValue
          ){
            $http({
                  method: 'GET',
                  url: '/retrieve/event/'+userEvent.getEvent()+
                    '/'+newValue+'/recruiter/'+$state.params.recruiterId+'/Busy/times'
            })
            .success(function(data, status, headers, config) {
                for( var i = 0; i < $scope.times.length; i++) {
                  if( $scope.times[i]['dateChanged'] ) {
                    $scope.times[i]['windowInterviewers'] = data;
                    $scope.times[i]['rangeInterviewers'] = data;
                    $scope.times[i]['dateChanged'] = false;
                  }
                }
            });
          }
        })
      });

      var formUrl = '/submit/event/'+userEvent.getEvent()+'/'+
        $state.params.recruiterId+'/available/time';

      document.getElementById("unavailableTimeForm").setAttribute('action',formUrl);

      $scope.addNewBusyTime = function() {
        var newItemNo = $scope.times.length+1;
        $scope.times.push({
          'id':'time'+newItemNo, dateChanged: false,
          'windowInterviewers': $scope.windowInterviewers,
          'rangeInterviewers': $scope.rangeInterviewers
        });
      }
});
