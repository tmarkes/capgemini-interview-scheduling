var mysql = require('mysql2');

var config =
{
    host     : process.env.CUSTOMCONNSTR_RDS_HOSTNAME,
    user     : process.env.CUSTOMCONNSTR_RDS_USERNAME,
    password : process.env.CUSTOMCONNSTR_RDS_PASSWORD,
    port     : process.env.CUSTOMCONNSTR_RDS_PORT,
    database : process.env.CUSTOMCONNSTR_RDS_DB_NAME,
    ssl: true
};

exports.findAllEvents = function(req, res) {
  var conn = new mysql.createConnection(config);

  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       conn.query('SELECT name, id from Event order by start_date, start_time', function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });

};
exports.findEventRooms = function(req, res) {
  var conn = new mysql.createConnection(config);

  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var eventRoomQuery = 'SELECT room, id from EventRooms where event_id = '+
         req.params.eventId;

       conn.query(eventRoomQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.findEventInfo = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var eventInfoQuery = "SELECT name, DATE_FORMAT(start_time, '%h:%i %p') as start_time,"+
         "DATE_FORMAT(end_time, '%h:%i %p') as end_time,start_date, "+
         "end_date from Event where id = "+req.params.eventId;

       conn.query(eventInfoQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.findEventRecruiters = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var eventRecQuery =
         "select " +
           "distinct recruiter_id as id, concat(allRecs.last_name, ', '," +
           "allRecs.first_name, ' ', substring(allRecs.middle_name,1,1)) as name " +
         "from " +
     	    "RegisteredEventRecruiters eventRecs " +
           "join Recruiter allRecs on eventRecs.recruiter_id = allRecs.id " +
         "where eventRecs.event_id = "+req.params.eventId+" "+
         "order by name";

       conn.query(eventRecQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.findSpecificallyAvailableEventRecruiters = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var eventRecQuery =
         "select " +
           "distinct allRecs.id, concat(allRecs.last_name, ', '," +
           "allRecs.first_name, ' ', substring(allRecs.middle_name,1,1)) as name " +
         "from " +
           "RegisteredEventRecruiters eventRecs " +
           "join Recruiter allRecs on eventRecs.recruiter_id = allRecs.id " +
           "join AppointmentSchedule app on eventRecs.recruiter_id = app.recruiter_id " +
             "and eventRecs.event_id = app.event_id " +
         "where eventRecs.event_id = "+req.params.eventId+" "+
           "and allRecs.id != "+req.params.recruiterId+" "+
           "and app.start_date = '"+req.params.intStartDate+"' "+
           "and app.start_time = '"+req.params.intStartTime+"' "+
           "and app.appointment_desc = 'available' "+
         "order by name";

       conn.query(eventRecQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.findAvailableInterviewTimes = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var evRecCalQuery =
         "select distinct r.id as id, app.start_time as timeId, " +
           "concat(DATE_FORMAT(app.start_time, '%h:%i %p'),' (',concat(r.last_name, ', ',r.first_name, ' ', substring(r.middle_name,1,1)),')') as startTime " +
           "from " +
             "Event e " +
             "inner join RegisteredEventRecruiters re on e.id = re.event_id " +
             "inner join Recruiter r on r.id = re.recruiter_id " +
             "inner join AppointmentSchedule app on app.event_id = e.id and app.recruiter_id = r.id " +
         "where app.appointment_desc = 'available' " +
           "and app.start_date = '"+req.params.startDate+"' "+
           "and e.id = "+req.params.eventId+" "+
         "order by app.start_time";

       conn.query(evRecCalQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.findRecruiterTimes = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var evRecTimeQuery =
         "select distinct r.id as id, app.start_time as timeId,  app.end_time as endTimeId, " +
           "DATE_FORMAT(app.start_time, '%h:%i %p') as startTime, " +
           "DATE_FORMAT(app.end_time, '%h:%i %p') as endTime " +
           "from " +
       	    "Event e " +
             "inner join RegisteredEventRecruiters re on e.id = re.event_id " +
             "inner join Recruiter r on r.id = re.recruiter_id " +
             "inner join AppointmentSchedule app on app.event_id = e.id and app.recruiter_id = r.id " +
         "where app.appointment_desc = '"+req.params.timeType+"' " +
           "and app.start_date = '"+req.params.startDate+"' "+
           "and e.id = "+req.params.eventId+" "+
           "and r.id = "+req.params.recruiterId+" "+
         "order by app.start_time";

       conn.query(evRecTimeQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.findRecruiterInterviews = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var evRecIntvwQuery =
         "select concat(re.start_date,' at ',DATE_FORMAT(re.start_time, '%h:%i %p'),"+
          "' (',evr.room,') w/ ',c.first_name,' ',c.last_name) as interview, " +
          "evr.room as room, re.interview_rm_id as roomId " +
           "from " +
             "AppointmentSchedule re " +
             "inner join Candidate c on c.id = re.candidate_id " +
             "inner join EventRooms evr on evr.id = re.interview_rm_id and evr.event_id = re.event_id " +
         "where app.appointment_desc = 'Interview' " +
           "and re.event_id = "+req.params.eventId+" "+
           "and recruiter_id = "+req.params.recruiterId+" "+
         "order by re.start_date, re.start_time";

       conn.query(evRecIntvwQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.findEventRecruiterCalendar = function(req, res) {
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var evRecCalQuery =
         "select " +
           "appointment_desc, concat('"+'process.env.RESUME_BUCKET'+"/',c.resume_dl_link) as resume, year(start_date) as start_year, month(start_date) as start_month, day(start_date) as start_day, " +
           "hour(start_time) as start_hour, minute(start_time) as start_min, " +
           "year(end_date) as end_year, month(end_date) as end_month, day(end_date) as end_day, " +
           "hour(end_time) as end_hour, minute(end_time) as end_min " +
         "from AppointmentSchedule re " +
         "inner join Candidate c on c.id = re.candidate_id " +
         "where recruiter_id = "+req.params.recruiterId+" "+
           "and event_id = "+req.params.eventId+" "+
           "and appointment_desc != 'available'";

       conn.query(evRecCalQuery, function(err, rows, fields) {
         if (err) throw err;

         res.status(200).send(JSON.stringify(rows));
       });
    }
  });
};
exports.sendConfirmation = function(req, res) {
  var nodemailer = require("nodemailer");
  var conn = new mysql.createConnection(config);
  conn.connect(
    function (err) {
    if (err) {
        console.log("!!! Cannot connect !!! Error:");
        throw err;
    }
    else
    {
       console.log("Connection established.");
       var eventInfoQuery =
         "select " +
           "name, room " +
         "from " +
           "RegisteredEventRecruiters eventRecs " +
           "inner join Event allEvent on eventRecs.event_id = allEvent.id " +
           "inner join EventRooms eventRms on eventRecs.event_id = eventRms.event_id and " +
             "eventRecs.def_interview_room_id = eventRms.id "
         "where eventRecs.event_id = "+req.params.eventId+" "+
           "and eventRecs.recruiter_id = "+req.params.intId;

       conn.query(eventInfoQuery, function(err, rows, fields) {
         if (err) throw err;
         var eventName = '';
         var eventRoom = '';
         eventName = rows[0]['name'];
         eventRoom = rows[0]['room'];

         var recruiterInfoQuery = "SELECT first_name, last_name, email_id from Recruiter where "+
           "id = "+req.params.intId;

         conn.query(recruiterInfoQuery, function(err, rows, fields) {
           if (err) throw err;
           var recruiterFirstName = '';
           var recruiterLastName = '';
           var recruiterEmailId = '';
           recruiterFirstName = rows[0]['first_name'];
           recruiterLastName = rows[0]['last_name'];
           if( rows[0]['email_id'].includes("@capgemini.com") ) {
             recruiterEmailId = rows[0]['email_id'];
           } else {
             recruiterEmailId = rows[0]['email_id']+"@capgemini.com";
           }

           var smtpTransport = nodemailer.createTransport("SMTP", {
              service: process.env.CUSTOMCONNSTR_PROCESS_SERVICE,  // sets automatically host, port and connection security settings
              auth: {
                  user: process.env.CUSTOMCONNSTR_CONFIRM_EMAIL,
                  pass: process.env.CUSTOMCONNSTR_CONFIRM_PASSWORD
              }
           });

           smtpTransport.sendMail({  //email options
              from: "Capgemini Interviewing <cginstsch@gmail.com>", // sender address.  Must be the same as authenticated user if using Gmail.
              to: req.params.candFName+" "+req.params.candLName+" <"+req.params.candEmail+">", // receiver
              subject: "["+eventName+"] Capgemini "+req.params.intStart+" Interview Confirmation", // subject
              text: "Hello! Thank you for your "+eventName+" Capgemini interview signup. You have been confirmed "+
               "for an interview with "+recruiterFirstName+" "+recruiterLastName+" at "+req.params.intStart+
               " on "+req.params.intDate+" in "+eventRoom+". Please notify the Capgemini event coordinator if you have any questions or "+
               "concerns. We look forward to interviewing you.", // body
              html: "Hello! <p>Thank you for your "+eventName+" Capgemini interview signup.</p> <p>You have been confirmed "+
               "for an interview with "+recruiterFirstName+" "+recruiterLastName+" at "+req.params.intStart+
               " on "+req.params.intDate+" in "+eventRoom+". Please notify the Capgemini event coordinator if you have any questions or "+
               "concerns.</p> <p>We look forward to interviewing you.</p>"
           }, function(error, response){  //callback
               if(error){
                  console.log(req.params.candEmail+error);
               }
           });
           smtpTransport.sendMail({  //email options
             from: "Capgemini Interviewing <cginstsch@gmail.com>", // sender address.  Must be the same as authenticated user if using Gmail.
             to: recruiterFirstName+" "+recruiterLastName+" <"+recruiterEmailId+">", // receiver
             subject: "["+eventName+"] "+req.params.intDate+" "+req.params.intStart+" Interview: "+
               req.params.candFName+" "+req.params.candLName, // subject
             text: "Hello! "+req.params.candFName+" "+req.params.candLName+" has scheduled an interview with you for the "+eventName+"."+
              "The interview will be at "+req.params.intStart+" on "+req.params.intDate+" in "+eventRoom+". Please notify "+
              "the Capgemini event coordinator if you have any questions or concerns. ", // body
             html: "Hello! <p>"+req.params.candFName+" "+req.params.candLName+" has scheduled an interview with you for the "+eventName+". "+
              "The interview will be at "+req.params.intStart+" on "+req.params.intDate+" in "+eventRoom+".</p> <p>Please notify "+
              "the Capgemini event coordinator if you have any questions or concerns.</p>"
           }, function(error, response){  //callback
               if(error){
                  console.log(recruiterEmailId+error);
               }
           });
           smtpTransport.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
         });
       });
    }
  });
};
