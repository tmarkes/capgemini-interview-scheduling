(function(angular) {
  "use strict";

angular.module('scheduleApp',
  ['ui.router', 'CalendarController','EventCreationController', 'CandidateController'])
  .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise("/");
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $stateProvider
      .state('flowSelect', {
        url: "/",
        controller: "FlowSelectCtrl",
        templateUrl: "/tpls/flow-selection.tpl"
      })
      .state('intervewierFlowSelect', {
        url: "/loggedInterviewer",
        controller: "InterviewerFlowSelectCtrl",
        templateUrl: "/tpls/interviewer-flow-selection.tpl"
      })
      .state('login', {
        url: "/interviewerEvent",
        controller: "LoginCtrl",
        templateUrl: "/tpls/choose_menu.tpl",
        params: {
          interviewerView: null
        }
      })
      .state('interviewerList', {
        url: "/interviewerList",
        controller: "EventInterviewersCtrl",
        templateUrl: "/tpls/choose_menu.tpl",
        preventHomeReExecution: true,
        params: {
          eventId: null,
          interviewerView: null
        }
      })
      .state('chooseEvent', {
        url: "/event",
        controller: "ChooseEventCtrl",
        templateUrl: "/tpls/choose_menu.tpl"
      })
      .state('scheduleInterview', {
        url: "/interviewSetup",
        controller: "AddInterviewCtrl",
        templateUrl: "/tpls/setup-interview.tpl",
        preventHomeReExecution: true,
        params: {
          eventId: null
        }
      })
      .state('viewInterviewerCalendar', {
        url: "/event/:eventId/:recruiterId/:recruiterName/recruiterCalendar",
        controller: "calendarCtlr",
        templateUrl: "/tpls/calendar_view.tpl",
        params: {
          eventId: null,
          recruiterId: null,
          recruiterName: null
        }
      })
      .state('interviewConfirm', {
        url: "/thankyou",
        controller: "InterviewConfirmCtrl",
        templateUrl: "/tpls/interview-confirmed.tpl"
      })
      .state('setupEvent', {
        url: "/new/setup/event",
        controller: "EventCreationCtrl",
        templateUrl: "/tpls/setup-event.tpl"
      })
      .state('addRecruiter', {
        url: "/new/setup/eventRec",
        controller: "RecruiterAddCtrl",
        templateUrl: "/tpls/add-event-recruiters.tpl"
      })
      .state('viewInterviewers', {
        url: "/submit/event/:eventId/busytime",
        controller: "ViewInterviewersCtrl",
        templateUrl: "/tpls/choose_menu.tpl",
        preventHomeReExecution: true
      })
      .state('submitUnavailableTimes', {
        url: "/submit/busytimeRec",
        controller: "SubmitUnavailableCtrl",
        templateUrl: "/tpls/setup-busytime.tpl",
        params: {
          eventId: null,
          recruiterId: null,
          recruiterName: null
        }
      })
      .state('resetAvailableTimes', {
        url: "/submit/event/:eventId/:recruiterId/:recruiterName/availableTimes",
        controller: "busyTimeCalendarCtrl",
        templateUrl: "/tpls/setup-availtime.tpl",
        params: {
          eventId: null,
          recruiterId: null,
          recruiterName: null
        }
      })
      .state('modifyInterview', {
        url: "/event/:eventId/:recruiterId/modify",
        controller: "InterviewModificationCtrl",
        templateUrl: "/tpls/modify-interview.tpl"
      })
      .state('eventSubmissionSuccess', {
        url: "/event/:eventId/eventSubmitted",
        controller: "EventConfirmCtrl",
        templateUrl: "/tpls/event-confirmed.tpl"
      })
      .state('timeSubmissionSuccess', {
        url: "/timeSubmitted",
        controller: "TimeConfirmCtrl",
        templateUrl: "/tpls/times-confirmed.tpl"
      })
  })
  .directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
           element.datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {
                    if( element.attr("name") === 'interviewDate' ) {
                      scope.interview.date = date;
                    } else if( element.attr("name") === 'eventStartDate' ) {
                      scope.eventObj.startDate = date;
                    } else if( element.attr("name") === 'eventEndDate' ) {
                      scope.eventObj.endDate = date;
                    } else if( element.attr("name") === 'busyDate' ) {
                      scope.unavailableTimeForm.busyDate = date;
                    }
                    scope.$apply();
                }
            });
        }
    };
  })
  .directive('candidateResume', function() {
    return {
      restrict: 'AE',
      scope: {
        file: '@'
      },
      link: function(scope, el, attrs){
        el.bind('change', function(event){
          var files = event.target.files;
          var file = files[0];
          scope.file = file;
          scope.$parent.file = file;
          scope.$apply();
        });
      }
    };
  })
  .factory("userEvent", function() {
      var eventSet;

      var addEvent = function(val) {
        eventSet=val;
      }

      var getEvent = function(){
          return eventSet;
      }

      return {
        addEvent : addEvent ,
        getEvent : getEvent
      };
  })
  .controller("FlowSelectCtrl", function($scope, $state) {
      $scope.navigateToEventSelect = function() {
        $state.go("chooseEvent");
      }

      $scope.navigateToInterviewerMenu = function() {
          $state.go("chooseEvent");
      }
  })
  .controller("LoginCtrl", function($scope, $http, $state, userEvent) {
    console.log( "LoginCtrl: " + $state.params.interviewerView);
    $http({
          method: 'GET',
          url: '/retrieve/event'
    })
    .success(function(data, status, headers, config) {
         $scope.items = data;
         $scope.selectionItem = "your Recruiting Event";
    });

    $scope.processChoice = function(itemId,itemName) {
      userEvent.addEvent(itemId);
      $state.go("interviewerList", { eventId : itemId,
       interviewerView: $state.params.interviewerView });
    }
  })
  .controller("EventInterviewersCtrl", function($scope, $state, $http, $stateParams, userEvent) {
      userEvent.addEvent($stateParams.eventId);
      console.log( "EventInterviewersCtrl: " + $stateParams.interviewerView);
      $http({
            method: 'GET',
            url: '/retrieve/event/'+userEvent.getEvent()+'/recruiters'
      })
      .success(function(data, status, headers, config) {
           $scope.items = data;
           $scope.selectionItem = "your name to view your event calendar";
           userEvent.addEvent($stateParams.eventId);
      });

      $scope.processChoice = function(itemId,itemName) {
        if( $stateParams.interviewerView == 'calendar' ) {
          $state.go("resetAvailableTimes", {
            recruiterId : itemId, eventId : userEvent.getEvent(), recruiterName : itemName });
        } else if( $stateParams.interviewerView == 'modify' ) {
          $state.go("modifyInterview", {
            recruiterId : itemId, eventId : userEvent.getEvent(), recruiterName : itemName });
        }
      }
  })
  .controller("InterviewerFlowSelectCtrl", function($scope, $state) {
      $scope.navigateToEventCreation = function() {
        $state.go("setupEvent");
      }

      $scope.navigateToInterview = function(viewSelection) {
        console.log( "InterviewerFlowSelectCtrl: " + viewSelection);
        $state.go("login", {
          interviewerView : viewSelection });
      }
  });

})(angular);
