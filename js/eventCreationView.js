angular.module("EventCreationController", [])
  .controller("EventCreationCtrl", function($scope, $state, $http, userEvent) {
      $scope.saveEventDetails = function(eventObj) {
        var detailsUrl = '/submit/event/'+$scope.eventObj.eventName+
          '/'+$scope.eventObj.startDate+'/'+$scope.eventObj.endDate+
          '/'+$scope.eventObj.startTime+'/'+$scope.eventObj.endTime+
          '/'+$scope.eventObj.eventRooms;
          $http({
                method: 'POST',
                url: detailsUrl
          })
          .success(function(data, status, headers, config) {
              var eventId = data[0]['eid'];
              userEvent.addEvent(eventId);
              $state.go("addRecruiter");
          })
          .error(function(resp) {
            console.log(resp);
            alert("There was an error submitting your event. Please try again.")
          });
      }
  })
  .controller("RecruiterAddCtrl", function($scope, $state, $http, userEvent) {
      $http({
            method: 'GET',
            url: '/retrieve/'+userEvent.getEvent()+'/eventRooms'
      })
      .success(function(data, status, headers, config) {
           $scope.interviewRooms = data;
      });

      $scope.recruiters = [{id: 'recruiter1'}];
      var formUrl = '/submit/event/'+userEvent.getEvent()+'/recruiters';

      document.getElementById("addEventRecruitersForm").setAttribute('action',formUrl);

      $scope.addNewRecruiter = function() {
        var newItemNo = $scope.recruiters.length+1;
        $scope.recruiters.push({'id':'recruiter'+newItemNo});
      }
  })
  .controller("EventConfirmCtrl", function($scope, $state, $location, $stateParams) {
    var busyTimePath = "/#/submit/event/"+$stateParams.eventId+"/busytime";
    $scope.busytimeURL = $location.host()+busyTimePath;
    $scope.busytimePath = busyTimePath;
  })
  .controller("ViewInterviewersCtrl", function($scope, $state, $http, $stateParams, userEvent) {
      userEvent.addEvent($stateParams.eventId);
      $http({
            method: 'GET',
            url: '/retrieve/event/'+userEvent.getEvent()+'/recruiters'
      })
      .success(function(data, status, headers, config) {
           $scope.items = data;
           $scope.selectionItem = "your name to submit unavailability for interviews";
           userEvent.addEvent($stateParams.eventId);
      });

      $scope.processChoice = function(itemId,itemName) {
        $state.go("submitUnavailableTimes", {
          recruiterId : itemId, eventId : userEvent.getEvent(), recruiterName : itemName });
      }
  })
  .controller("InterviewModificationCtrl", function($scope, $state, $http, userEvent) {
    $http({
          method: 'GET',
          url: '/retrieve/event/'+userEvent.getEvent()
    })
    .success(function(data, status, headers, config) {
         $scope.eventName = data[0]['name'];
         $scope.eventStartDate = data[0]['start_date'].split("T")[0];
         $scope.eventEndDate = data[0]['end_date'].split("T")[0];
         $scope.eventStartTime = data[0]['start_time'];
         $scope.eventEndTime = data[0]['end_time'];
    });
  })
  .controller("SubmitUnavailableCtrl", function ($scope, $state, $http, userEvent) {
    $scope.interviewerName = $state.params.recruiterName;
    $scope.specificTimeWindow = 'Specific Time Window';
    $scope.timeRange = 'Time Range';
    $http({
          method: 'GET',
          url: '/retrieve/event/'+userEvent.getEvent()
    })
    .success(function(data, status, headers, config) {
         $scope.eventName = data[0]['name'];
         $scope.eventStartDate = data[0]['start_date'].split("T")[0];
         $scope.eventEndDate = data[0]['end_date'].split("T")[0];
         $scope.eventStartTime = data[0]['start_time'];
         $scope.eventEndTime = data[0]['end_time'];
    });

    $scope.$watch("unavailableTimeForm.busyDate", function(newValue, oldValue){
      $(".newTimeslotForm").each(function() {
        if((
           $(this).find(".interviewStartTime :selected").text().includes("Select One")
        || $(this).find(".interviewRangeStartTime :selected").text().includes("Select One")
        || $(this).find(".interviewRangeEndTime :selected").text().includes("Select One")
            )
           && $(this).find(".busyDate").text() !== oldValue
        ){
          $http({
                method: 'GET',
                url: '/retrieve/event/'+userEvent.getEvent()+
                  '/'+newValue+'/recruiter/'+$state.params.recruiterId+'/available/times'
          })
          .success(function(data, status, headers, config) {
              for( var i = 0; i < $scope.times.length; i++) {
                if( $scope.times[i]['dateChanged'] ) {
                  $scope.times[i]['windowInterviewers'] = data;
                  $scope.times[i]['rangeInterviewers'] = data;
                  $scope.times[i]['dateChanged'] = false;
                }
              }
          });
        }
      })
    });

    $scope.times = [{
      id: 'time1', dateChanged: false, windowInterviewers: $scope.windowInterviewers,
      rangeInterviewers: $scope.rangeInterviewers
    }];
    var formUrl = '/submit/event/'+userEvent.getEvent()+'/'+
      $state.params.recruiterId+'/Busy/time';

    document.getElementById("unavailableTimeForm").setAttribute('action',formUrl);

    $scope.addNewBusyTime = function() {
      var newItemNo = $scope.times.length+1;
      $scope.times.push({
        'id':'time'+newItemNo, dateChanged: false,
        'windowInterviewers': $scope.windowInterviewers,
        'rangeInterviewers': $scope.rangeInterviewers
      });
    }
  })
  .controller("TimeConfirmCtrl", function($scope, $state) {})
  .controller("InterviewConfirmCtrl", function($scope, $state) {});
