<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form class="form-horizontal" role="form">
				<div class="form-group">

					<label for="inputEmail3" class="col-sm-2 control-label">
						SSO User Name
					</label>
					<div class="col-sm-10">
						<input type="text" ng-model="login.username" class="form-control" required>
					</div>
				</div>
				<div class="form-group">

					<label for="inputPassword3" class="col-sm-2 control-label">
						Password
					</label>
					<div class="col-sm-10">
						<input type="password" ng-model="login.password" class="form-control" required>
					</div>
				</div>
				<!--<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<div class="checkbox">

							<label>
								<input type="checkbox"> Remember me
							</label>
						</div>
					</div>
				</div>-->
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">

						<button type="button" ng-click="login()" class="btn btn-default">
							Sign in
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
