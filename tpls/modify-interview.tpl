<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row subtitle-row-large">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center font-normal subtitle-normal">
      Modify or cancel your interviews!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
	<div class="row">
		<div class="col-sm-12 text-center text-success font-normal paragraph-text">
			<b>Event:</b> {{eventName}}<br />
			<b>Dates:</b> {{eventStartDate}} <b>through</b> {{eventEndDate}}<br />
			<b>Times:</b> {{eventStartTime}} - {{eventEndTime}} ET
		</div>
	</div>
  <form name="setupInterview" class="form-font" novalidate>
			<div class="row text-center interview-row">
				<label class="col-sm-7 form-rowA">Your Event Interviews:</label>
				<select class="col-md-2 text-center" ng-model="interview.selection" name="interviewSelected"
						ng-options="interview.interview for interview in interviews">
						<option value="">Select One</option>
				</select>
			</div>
			<div class="row text-center interview-row">
				<label class="col-md-5 form-rowB busytimeForm-unavailType">Modify or Cancel?</label>
				<fieldset>
					<input id="busytimeForm-unavailTypeOptionOne" ng-click="modify = true" name="formView" type="radio">{{modifyOption}}</input>
					<input id="busytimeForm-unavailTypeOptionTwo" ng-click="modify = false" name="formView" type="radio">{{cancelOption}}</input>
				</fieldset>
			</div>
			<div ng-cloak ng-show="modify" class="row text-center interview-row">
          <label class="col-sm-7 form-rowA">New Room Number?</label>
					<select class="col-md-2 text-center" ng-model="interview.room" name="interviewRoom"
							ng-options="interview.room for interview in interviews "+
							"track by interview.roomId">
							<option value="">Select One</option>
					</select>
      </div>
			<div ng-cloak ng-show="modify" class="row text-center interview-row">
					<label class="col-sm-7 form-rowB">Different Interviewer?</label>
					<select class="col-md-2 text-center" ng-model="interview.newRecruiter" name="differentInterviewer"
							ng-options="availableRecruiter.name for availableRecruiter in availableRecruiters "+
							"track by availableRecruiter.id">
							<option value="">Select One</option>
					</select>
			</div>
      <div ng-show="modify" class="row text-center interview-row">
        <label class="col-sm-7 form-rowA">New Interview Date?</label>
				<input class="col-md-2" name="interviewDate" type="text" ng-model="interview.date" jqdatepicker>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowA">Available Interview Times:</label>
				<select class="col-md-2 text-center" ng-model="interview.startTime" name="interviewStart"
						ng-options="interviewer.startTime for interviewer in interviewers
						track by interviewer.id+interviewer.timeId">
						<option value="">Select One</option>
				</select>
			</div>
      <div ng-hide="modify" class="row text-center interview-row">
        <label class="col-sm-7 form-rowB">Cancel Interiew?</label>
				<fieldset>
					<input id="cancelInterview-OptionOne" ng-click="cancelInterview = false" name="cancelInterview" type="radio">{{cancelNo}}</input>
					<input id="cancelInterview-OptionTwo" ng-click="cancelInterview = true" name="cancelInterview" type="radio">{{cancelYes}}</input>
				</fieldset>
			</div>
			<div class="row text-center interview-row">
				<button type="button" ng-click="saveModificationDetails(interview, cancelInterview)">Submit</button>
			</div>
    </form>
  </div>
</div>
