<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-normal" />
    <div class="col-sm-12 text-center text-success font-normal subtitle-normal">
      Thank you for your recruiting event time submission!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
<!--	<div class="row">
    <div class="col-md-12 text-center text-success"  style="margin-top:10px">
			Please use the following link to view your event activities:<br />
			<a href="{{busytimeURL}}">{{busytimeURL}}</a>
    </div>
  </div>-->
	<div class="row">
    <div class="col-sm-12 text-center text-success font-normal paragraph-text-normal">
      Hope you have a wonderful event!
    </div>
  </div>
	<div class="row">
    <a href="https://www.capgemini.com/" class="col-sm-12 text-center text-success font-normal link">
      https://www.capgemini.com/
    </a>
  </div>
</div>
