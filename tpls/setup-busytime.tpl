<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center text-success font-normal subtitle-normal">
      Time Page for {{interviewerName}}<br />Please submit your unavailability for these 30-minute interviews!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
	<div class="row">
		<div class="col-sm-12 text-center text-success font-normal paragraph-text">
			<b>Event:</b> {{eventName}}<br />
			<b>Dates:</b> {{eventStartDate}} <b>through</b> {{eventEndDate}}<br />
			<b>Times:</b> {{eventStartTime}} - {{eventEndTime}} ET
		</div>
	</div>
	<form id="unavailableTimeForm" action="" name="unavailableTimeForm" class="form-font" method="POST">
	  <fieldset class="newTimeslotForm" name="newTimeslotForm" ng-repeat="time in times" id="{{time.id}}">
			<div class="row text-center interview-row form-rowA">
				<label>New Unavailability Window:</label>
			</div>
			<div class="row text-center interview-row">
				<label class="col-sm-7 form-rowB">Date: </label>
				<input class="col-md-2 busyDate"  ng-click="time.dateChanged = true" name="busyDate" type="text" ng-model="newTimeslotForm.busyDate" jqdatepicker/>
			</div>
			<div class="row text-center interview-row">
				<label class="col-md-5 form-rowB busytimeForm-unavailType">Unavailability Type: </label>
				<fieldset>
					<input id="busytimeForm-unavailTypeOptionOne" ng-click="busytimeRange = false" name="busytimeType" type="radio">{{specificTimeWindow}}</input>
					<input id="busytimeForm-unavailTypeOptionTwo" ng-click="busytimeRange = true" name="busytimeType" type="radio">{{timeRange}}</input>
				</fieldset>
			</div>
			<div ng-cloak ng-hide="busytimeRange" class="row text-center interview-row">
				<label class="col-sm-7 form-rowA">Unavailable Window: </label>
				<select class="col-md-2 text-center interviewStartTime" ng-model="interview.startTime" name="interviewStart"
						ng-options="windowInterviewer.startTime for windowInterviewer in time.windowInterviewers
						track by windowInterviewer.timeId">
						<option value="">Select One</option>
				</select>
			</div>
			<div ng-cloak ng-show="busytimeRange" class="row text-center interview-row">
				<label class="col-sm-7 form-rowA">Unavailability Range Start Time: </label>
				<select class="col-md-2 text-center interviewRangeStartTime" ng-model="interview.rangeStartTime" name="interviewRangeStart"
						ng-options="rangeInterviewer.startTime for rangeInterviewer in time.rangeInterviewers
						track by rangeInterviewer.timeId">
						<option value="">Select One</option>
				</select>
			</div>
			<div ng-cloak ng-show="busytimeRange" class="row text-center interview-row">
				<label class="col-sm-7 form-rowB">Unavailability Range End Time: </label>
				<select class="col-md-2 text-center interviewRangeEndTime" ng-model="interview.rangeEndTime" name="interviewRangeEnd"
						ng-options="rangeInterviewer.endTime for rangeInterviewer in time.rangeInterviewers
						track by rangeInterviewer.endTimeId">
						<option value="">Select One</option>
				</select>
			</div>
	  </fieldset>
		<div class="row text-center interview-row">
			<button type="button" ng-click="addNewBusyTime()">Add More</button>
		</div>
		<div class="row text-center interview-row">
			<button id="timeFormButton" type="submit">Submit All Entries</button>
		</div>
	</form>
</div>
