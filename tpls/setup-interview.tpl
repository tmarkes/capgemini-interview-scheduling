<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row subtitle-row-large">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center font-normal subtitle-normal">
      Schedule a 30-minute interview!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
  <form name="setupInterview" class="form-font" novalidate>
      <div ng-cloak class="row text-center interview-row">
          <label class="col-sm-7 form-rowA">Date:</label>
					<input class="col-md-2" name="interviewDate" type="text" ng-model="interview.date" jqdatepicker>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowB">Available Interviews:</label>
				<select class="col-md-2 text-center" ng-model="interview.startTime" name="interviewStart"
						ng-options="interviewer.startTime for interviewer in interviewers
						track by interviewer.id+interviewer.timeId">
						<option value="">Select One</option>
				</select>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowA">Candidate First Name:</label>
        <input class="col-md-2" name="candidateFirstName" type="text" ng-model="candidate.firstname" required/>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowB">Candidate Middle Name:</label>
        <input class="col-md-2" name="candidateMiddleName" type="text" ng-model="candidate.middlename" required/>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowA">Candidate Last Name:</label>
        <input class="col-md-2" name="candidateLastName" type="text" ng-model="candidate.lastname" required/>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowB">Candidate Phone:</label>
        <input class="col-md-2" name="candidatePhone" type="text" ng-model="candidate.phone" required/>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowA">Candidate Email:</label>
        <input class="col-md-2" name="candidateEmail" type="text" ng-model="candidate.email" required/>
      </div>
      <div class="row text-center interview-row">
        <label class="col-sm-7 form-rowB">Candidate Resume:</label>
        <input class="col-md-2" id="candidateResume" name="candidateResume" type="file" candidateResume required/>
      </div>
			<div class="row text-center interview-row">
				<button type="button" ng-click="saveInterviewDetails(interview, candidate)">Submit</button>
			</div>
    </form>
  </div>
</div>
