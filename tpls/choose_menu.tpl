<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row subtitle-row-med">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center text-success font-normal subtitle-normal">
      Please select {{selectionItem}}:
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
  <div class="row">
    <div class="list-group font-normal">
      <a ng-click="processChoice(item.id,item.name)" class="list-group-item" ng-repeat="item in items">{{item.name}}</a>
    </div>
  </div>
</div>
