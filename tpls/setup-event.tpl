<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row subtitle-row-large">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center text-success font-normal subtitle-normal">
      Schedule an event!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
  <form name="setupEvent" class="form-font" novalidate>
		<div class="row text-center interview-row">
			<label class="col-sm-7 form-rowA">Event Name:</label>
			<input class="col-md-2" name="eventName" type="text" ng-model="eventObj.eventName" required/>
		</div>
		<div ng-cloak class="row text-center interview-row">
			<label class="col-sm-7 form-rowB">Start Date:</label>
			<input class="col-md-2" name="eventStartDate" type="text" ng-model="eventObj.startDate" jqdatepicker>
		</div>
    <div ng-cloak class="row text-center interview-row">
      <label class="col-sm-7 form-rowA">End Date:</label>
			<input class="col-md-2" name="eventEndDate" type="text" ng-model="eventObj.endDate" jqdatepicker>
    </div>
		<div ng-cloak class="row text-center interview-row">
			<label class="col-sm-7 form-rowB">Start Time (military):</label>
			<input class="col-md-2" placeholder="13:00:00" name="eventStartTime" type="text" ng-model="eventObj.startTime" required/>
		</div>
		<div ng-cloak class="row text-center interview-row">
			<label class="col-sm-7 form-rowA">End Time (military):</label>
			<input class="col-md-2" placeholder="18:00:00" name="eventEndTime" type="text" ng-model="eventObj.endTime" required/>
		</div>
		<div class="row text-center interview-row">
			<label class="col-sm-7 form-rowB">Comma-delimited Interview Room List:</label>
			<input class="col-md-2" name="eventRooms" type="text" ng-model="eventObj.eventRooms" required/>
		</div>
		<div class="row text-center interview-row">
			<button type="button" ng-click="saveEventDetails(eventObj)">Continue to add Recruiters</button>
		</div>
  </form>
</div>
