<div class="container-fluid">
	<div class="row">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center subtitle-big">
      Welcome, Capgemini employee!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
	<div class="row menu-btn-group-row">
		<div class="col-md-11 text-center">
			<form class="form-horizontal" name="flowSelect" role=form novalidate>
				<div class="btn-group-vertical menu-btn-group">
					<button class="menu-btn-group-first" type="button" ng-click="navigateToEventCreation()">Create Event</button>
					<button class="menu-btn-group-first" type="button" ng-click="navigateToInterview('calendar')">View Interview Schedule</button>
					<button type="button" ng-click="navigateToInterview('modify')">Modify/Cancel Interviews</button>
				</div>
			</form>
		</div>
	</div>
</div>
