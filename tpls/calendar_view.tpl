<div ng-controller="calendarCtlr" class="span5 center-table">
    <table>
        <tr>
            <th>
              <div class="col-md-4" style="margin-bottom: 10px; margin-top: 10px;color: darkblue">
                Calendar of {{interviewer.name}}
              </div>
            </th>
        </tr>
        <tr>
            <th>
                <div class="calendar" ui-calendar="calOptions" ng-model="eventOptions" ng-click="eventClicked()"></div>
            </th>
        </tr>
        <tr id="alertMessageWhenEventIsClicked"></tr>
    </table>
</div>
