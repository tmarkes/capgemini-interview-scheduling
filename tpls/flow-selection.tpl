<div class="container-fluid">
	<div class="row landing-page-title">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center subtitle-big">
      Welcome to the Capgemini Interview Scheduling Tool!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
	<div class="row menu-btn-group-row">
		<div class="col-md-11 text-center">
			<form class="form-horizontal" name="flowSelect" role=form novalidate>
				<div class="btn-group-vertical menu-btn-group">
					<button class="menu-btn-group-first" type="button" ng-click="navigateToEventSelect()">Candidate</button>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
    <div class="col-sm-12 text-center font-normal paragraph-text">
      To learn more about the opportunities at Capgemini,  and apply online, visit our careers site
			at<a href="https://www.capgemini.com/" class="col-md-12 text-center text-success link">
	      https://www.capgemini.com/
	    </a>
    </div>
  </div>
	<div class="row">
		<div class="col-sm-12 text-center font-normal paragraph-text">
			Thank you, and good luck!
			</a>
		</div>
	</div>
</div>
