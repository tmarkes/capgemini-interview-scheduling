<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row subtitle-row-small">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center text-success font-normal subtitle-normal">
      Add recruiters!
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
	<form id="addEventRecruitersForm" action="" name="addEventRecruitersForm" class="form-font" method="POST">
	  <fieldset name="newRecruitersForm" ng-repeat="recruiter in recruiters">
			<div class="row text-center interview-row">
				<label class="form-rowA">New Recruiter:</label>
			</div>
			<div class="row text-center interview-row">
				<label class="col-sm-7 form-rowB">First Name: </label>
				<input class="col-md-2" name="recruiterFirstName" type="text" ng-model="newRecruitersForm.firstName" required/>
			</div>
			<div ng-cloak class="row text-center interview-row">
				<label class="col-sm-7 form-rowA">Middle Name: </label>
				<input class="col-md-2" name="recruiterMiddleName" type="text" ng-model="newRecruitersForm.middleName" required/>
			</div>
	    <div ng-cloak class="row text-center interview-row">
				<label class="col-sm-7 form-rowB">Last Name: </label>
				<input class="col-md-2" name="recruiterLastName" type="text" ng-model="newRecruitersForm.lastName" required/>
	    </div>
			<div ng-cloak class="row text-center interview-row">
				<label class="col-sm-7 form-rowA">Cap Mailbox (ex: dave.smith, NOT 'dave.smith@capgemini.com'): </label>
				<input class="col-md-2" name="recruiterLogin" type="text" ng-model="newRecruitersForm.login" required/>
			</div>
			<div ng-cloak class="row text-center interview-row">
				<label class="col-sm-7 form-rowB">Default Interview Room:</label>
				<select class="col-md-2 text-center" ng-model="newRecruitersForm.interviewRoom" name="interviewRoom"
						ng-options="interviewRoom.room for interviewRoom in interviewRooms
						track by interviewRoom.id">
						<option value="">Select One</option>
				</select>
			</div>
	  </fieldset>
		<div class="row text-center interview-row">
			<button type="button" ng-click="addNewRecruiter()">Add More</button>
		</div>
		<div class="row text-center interview-row">
			<button id="recFormButton" type="submit">Submit All Entries</button>
		</div>
	</form>
</div>
