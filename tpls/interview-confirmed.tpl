<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center text-success header-main">
				Capgemini Scheduler
			</h2>
		</div>
	</div>
	<div class="row">
		<img src="images/Capgemini_external_icon_2017.jpg" class="cap-icon-small" />
    <div class="col-sm-12 text-center font-normal subtitle-normal">
      Thank you for your interest in interviewing with Capgemini.
    </div>
		<img src="images/HOLA_external_icon.png" class="hola-icon" />
  </div>
	<div class="row">
    <div class="col-sm-12 text-center text-success font-normal paragraph-text-event">
      With more than 180,000 people in over 40 countries, Capgemini is a
			global leader in consulting, technology and outsourcing services.
			Together with our clients, we create and deliver business, technology
			and digital solutions that fit their needs, enabling them to achieve
			innovation and competitiveness.
    </div>
  </div>
	<div class="row">
    <div class="col-sm-12 text-center text-success font-normal paragraph-text-normal">
      We invite you to visit our website to learn more about us.
    </div>
  </div>
	<div class="row">
    <a href="https://www.capgemini.com/" class="col-sm-12 text-center text-success font-normal link">
      https://www.capgemini.com/
    </a>
  </div>
</div>
