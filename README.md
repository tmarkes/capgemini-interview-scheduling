# capgemini-interview-scheduling

### Prerequisites

	Windows Instance (Local or EC2)

	Node.JS and NPM: https://www.npmjs.com/get-npm

	Command-Line Console: Windows Powershell (pre-installed)

	UI IDE: https://atom.io/

	Angular (installed via NPM): https://www.npmjs.com/package/@angular/cli

	GitHub (personal preference for GitHub Desktop application): https://desktop.github.com/

	Nodemailer (No need to download or install; this service is used to send emails from our previously created cgschint@gmail account): https://nodemailer.com/smtp/well-known/

### Running Locally (or on an EC2 instance)

Clone your own fork from https://gitlab.com/tmarkes/capgemini-interview-scheduling

cd capgemini-interview-scheduling

npm install

npm start

- Your app should now be running on localhost:8081

To have it run from a different port number, change the port number specified in [server.js](https://gitlab.com/tmarkes/capgemini-interview-scheduling/blob/master/server.js). The line is as follows:

    app.listen(8081);
  
Note: If application is running on an EC2 instance, you will need to configure:

	(1) In the Security Group(s) for your EC2 instance, an inbound TCP/IP rule to allow all traffic for your specified port
	
	(2) In the Windows Firewall settings on your EC2 instance, an inbound rule to allow all traffic for your specified port
	
- It may take an upwards of 30 minutes for the settings to take effect and for your port to be publicly accessible.

### MySQL Database Details

See the database model representation in [cgintsch-model.png] (https://gitlab.com/tmarkes/capgemini-interview-scheduling/blob/master/misc/cgintsch-model.png).

### Average Application Flow

- Lead Recruiter creates an Event and a list of Interviewers for the Event.

- Interviewers follow a link to submit their inavailability during the Event.

- Candidates follow a different link to reserve time with the above registered interviewers for a specific Event.

- Interviewers can login to the application to view their appointment schedule, modify interviews, and cancel interviews.

### Necessary Configuration Variable Updates

As the application currently stands set up for use in Elastic Beanstalk, the following 'process.env' variables will need replacements for migration to other environments:

| Environment Property   |                                    Explanation                                                | Location(s)              | 
| ---------------------- | --------------------------------------------------------------------------------------------- | ------------------------ |
| AWS_ACCESS_KEY_ID      | 1st AWS Account Access Key required for Amazon S3 communication with application. Private to your AWS account.              | retrieves.js, submits.js |
| AWS_SECRET_ACCESS_KEY  | 2nd AWS Account Access Key required for Amazon S3 communication with application. Private to your AWS account.              | retrieves.js, submits.js |
| CONFIRM_EMAIL          | cgintsch@gmail account used for sending interview confirmations to interviewers and attendees | retrieves.js             |
| CONFIRM_PASSWORD       | Password for the cginstsch@gmail account mentioned above                                       | retrieves.js             |
| PROCESS_SERVICE        | Identifier used by Nodemailer for particular service to use. Left as 'Gmail'.                 | retrieves.js             |
| RDS_DB_NAME            | Identifier for default database schema. Left as 'ebdb'.                                       | retrieves.js, submits.js |
| RDS_HOSTNAME           | Identifier for database hostname. Left as 'aa1op8ylanmgfqs.cwsygbp8rrem.us-east-1.rds.amazonaws.com'. | retrieves.js, submits.js |
| RDS_PASSWORD           | Identifier for database user pw. Left as 'Capgemini7ValuesHBTFTSMF'. | retrieves.js, submits.js |
| RDS_PORT           | Identifier for database connection port. Left as '3306'. | retrieves.js, submits.js |
| RDS_USERNAME           | Identifier for database user name. Left as 'cgintschmain'. | retrieves.js, submits.js |
| RESUME_BUCKET           | Identifier for Amazon S3 resume repository. Left as 'elasticbeanstalk-us-east-1-cgintsch-resumebucket-test'. | retrieves.js, submits.js |

