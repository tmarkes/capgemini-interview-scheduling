module.exports = function(app){
  var retrieves = require('./js/retrieves');
  app.get('/retrieve/event', retrieves.findAllEvents);
  app.get('/retrieve/:eventId/eventRooms', retrieves.findEventRooms);
  app.get('/retrieve/event/:eventId', retrieves.findEventInfo);
  app.get('/retrieve/event/:eventId/recruiters', retrieves.findEventRecruiters);
  app.get('/retrieve/event/:eventId/:intStartDate/:intStartTime/not/:recruiterId/recruiters',
    retrieves.findSpecificallyAvailableEventRecruiters);
  app.get('/retrieve/event/:eventId/:startDate/availableTimes', retrieves.findAvailableInterviewTimes);
  app.get('/retrieve/event/:eventId/recruiter/:recruiterId/interviews', retrieves.findRecruiterInterviews);
  app.get('/retrieve/event/:eventId/:startDate/recruiter/:recruiterId/:timeType/times',
    retrieves.findRecruiterTimes);
  app.get('/retrieve/event/:eventId/recruiter/:recruiterId/calendar',
    retrieves.findEventRecruiterCalendar);
  app.get('/sendEmails/:candEmail/:eventId/:intDate/:intStart/:intId/'+
    ':candFName/:candLName', retrieves.sendConfirmation);


  var submits = require('./js/submits');
  app.post('/submit/event/:eventId/interview/:intId/:intDate/:intStart/'+
      ':candFName/:candMName/:candLName/:candPhone/'+
      ':candEmail/:candResume', submits.submitInterviewDetails);
  app.post('/submit/event/:eventName/:eventStartDate/:eventEndDate/'+
      ':eventStartTime/:eventEndTime/:eventRoomList', submits.submitEventDetails);
  app.post('/submit/event/:eventId/recruiters', submits.submitEventRecruiters);
  app.post('/submit/event/:eventId/:recruiterId/:timeType/time', submits.submitRecruiterTime);
  app.post('/presigned/:filename/:filetype', submits.submitResume);
}
